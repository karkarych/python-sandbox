import turtle
  
  
myTurtle = turtle.Turtle()
myWin = turtle.Screen()  
myTurtle.begin_fill()
for x in range(4): 
    myTurtle.forward(150)
    myTurtle.right(90)
    myTurtle.forward(200)
    myTurtle.right(90)
    myTurtle.forward(50)
    myTurtle.right(90)
    myTurtle.forward(150)
    myTurtle.left(90)
    myTurtle.forward(100)
    myTurtle.left(90)
myTurtle.end_fill()
myWin.exitonclick()