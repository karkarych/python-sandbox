from sympy import symbols, solve
import matplotlib.pyplot as plt

# Constants declaration
x = symbols('x')

def interpretedFunction(arg, rawFunction):
    x = arg
    res = eval(rawFunction)
    return res

def toFloat(s):
    try:
        return float(s)
    except ValueError:
        num, denom = s.split('/')
        return float(num) / float(denom)

# Program Body
function = input("f(x) = ") or "x**2-2*x+0"
interpretedExpression = eval(function)

radicals = solve(interpretedExpression)

print(f"Radicals: {radicals}")

Ax = []
Ay = []

if len(radicals) == 1:
    for arg in range(round(toFloat(radicals[0])-10), round(toFloat(radicals[0])+10)):
        Ax.append(arg)
        Ay.append(interpretedFunction(arg, function))

else:
    for arg in range(round(toFloat(radicals[0])-10), round(toFloat(radicals[1])+10)):
        Ax.append(arg)
        Ay.append(interpretedFunction(arg, function))

plt.plot(Ax, Ay)
plt.grid(color='grey', linestyle='-', linewidth=0.5)
plt.xlabel('x')
plt.ylabel('f(x)')
plt.title(f'f(x) = {function.replace("**", "^").replace("*", "")}')

for i, rad in enumerate(radicals):
    plt.annotate(rad, (Ax[Ax.index(rad)], Ay[Ax.index(rad)]))
plt.show()