function = input("Enter your function: ") or 'x**2 + 2*x + 1'
args = input("Enter your argument(s): ") or '3 4 5 6 8'

args = args.split(' ')

args = list(map(float, args))
for arg in args:
    x = arg
    print(f'{arg}: \t {function.replace("x", str(arg)).replace("**", "^")} = {eval(function)}')