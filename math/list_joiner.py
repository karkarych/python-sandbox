lst1 = [0, 1, 4, 5, 7, 10, 13, 15]
lst2 = [2, 3, 6, 8, 9, 11, 12, 14]

lst = []
c1 = 0
c2 = 0
for i in range(len(lst1)+len(lst2)-1):
    if lst1[c1] > lst2[c2]:
        lst.append(lst2[c2])
        print(f'{i} - Added {lst2[c2]} from lst2')
        c2 = c2 + 1
    else:
        lst.append(lst1[c1])
        print(f'{i} - Added {lst1[c1]} from lst1')
        c1 = c1 + 1

print(lst)