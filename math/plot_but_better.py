import pandas as pd
import numpy as np
from random import randint
import matplotlib.pyplot as plt
length = 25
status = 0
data = {'x': [], 'y': [], 'px': [], 'py': []}
test = {'x': [], 'y': [], 'z': []}
for i in range(0, 25):
    data['x'].append(randint(0, 100))
    data['y'].append(randint(0, 100))

plt.scatter(data['x'], data['y'], c='blue')

z = np.polyfit(data['x'], data['y'], 1)
print(z)
p = np.poly1d(z)
plt.plot(data['x'], p(data['x']), "r--")

# while status != 1:
for x in range(0,100):
    px = x
    for y in range(0,100):
        py = y

        for x in data['x']:
            test['x'].append(x)
        test['x'].append(px)
        for y in data['y']:
            test['y'].append(y)
        test['y'].append(py)
        # print(test)
        # cav = input('Continue?')
        test['z'] = np.polyfit(test['x'], test['y'], 1)
        if test['z'][0] != z[0] and test['z'][1] != z[1]:
            z1 = test['z']
            test = {'x': [], 'y': [], 'z': []}
            print(f'({px},{py}) - {z} <> {z1} - FAIL - skipped')
        else:
            data['px'].append(px)
            data['px'].append(py)
            z1 = test['z']
            print(f'({px},{py}) - {z} <> {z1} - PASS - appended')
            test = {'x': [], 'y': [], 'z': []}
    # status = 1

plt.scatter(data['px'], data['py'], c='green')

plt.show()
