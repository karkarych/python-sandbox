import plotly.express as px
import pandas as pd

def genArray(len):
    arr = []
    for i in range(len+1):
        arr.append(i)
    return arr

initNumber = resultNumber = int(input("Initial number: "))

iterations = {
    "path" : [initNumber],
    "odd" : 0,
    "even" : 0
}

while (resultNumber != 1 or (iterations["even"] == 0 and iterations["odd"] == 0)):

    if (resultNumber % 2) == 0.0:
        resultNumber = resultNumber / 2
        iterations['even'] += 1
        iterations['path'].append(resultNumber)

    else:
        resultNumber = 3 * resultNumber + 1
        iterations['odd'] += 1
        iterations['path'].append(resultNumber)

print(f'{initNumber} downgraded to 1 in {iterations["odd"] + iterations["even"]} iterations [{iterations["even"]} even / {iterations["odd"]} odd]')
print(iterations['path'])

df = pd.DataFrame(dict(
    Iterations = genArray(iterations['odd'] + iterations['even']),
    Number = iterations['path']
))

fig = px.line(df, x="Iterations", y="Number", title='"3x+1" dispersion') 
fig.show()