from os import walk, stat
from tabulate import tabulate

f = []
for (dirpath, dirnames, filenames) in walk('./'):
    f.extend(filenames)
    break


# for fileName in f:
#     print(f"{fileName} - {stat(fileName).st_size} KB")

fileStats = []
for fileName in f:
    fileStats.append([fileName, f'{round(stat(fileName).st_size/1024, 2)} KB'])
# print(fileStats)

print(tabulate(fileStats, headers=['Name', 'Size'], tablefmt='orgtbl'))