import pathlib
from os import listdir
from os.path import isfile, join
from nudenet import NudeClassifier
mypath = pathlib.Path().absolute()
classifier = NudeClassifier()

onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]

print('\n')
for file in onlyfiles:
    if file.endswith('.jpeg') or file.endswith('.jpg'):
        stats = classifier.classify(file)
        print(file, ': \t', stats[file]['unsafe'])

# print(classifier.classify(onlyfiles, batch_size=4))

