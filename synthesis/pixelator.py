from PIL import Image
import easygui
import math

filePath = easygui.fileopenbox()
fileRes = easygui.enterbox("Expectable resolution: ")
# fileRes = int(math.sqrt(fileRes))
img = Image.open(filePath)
imgSmall = img.resize((16, 16), resample=Image.BILINEAR)
result = imgSmall.resize(img.size, Image.NEAREST)
result.show()
