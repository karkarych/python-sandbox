import requests
first_letters = input('Enter first characters: ')
APIkey = '96472e31-1899-438e-aff7-882ac74ddeee'
params = {"apikey": APIkey, "lang": "ru", "text": ""}
url = 'https://api.wordassociations.net/associations/v1.0/json/search'

dict = open("russian.txt")
content = dict.read()
words = content.splitlines()

assoc = []
for word in words:
    word = word.lower()
    if word.startswith(first_letters) and len(word) > 4:
        params['text'] = word
        assoc_new = []
        r = requests.get(url, params=params).json()
        if len(r['response'][0]['items']) > 0:
            for ass in r['response'][0]['items']:
                assoc_new.append(ass['item'].lower())
            assoc_new.sort()
            assoc.sort()
            if assoc_new != assoc:
                assoc = assoc_new

                print(word, ': ', ", ".join(assoc_new), '\n\n')