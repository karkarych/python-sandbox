import requests
import os
import re

numbers = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12',
           '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25']
URL = "https://random-word-api.herokuapp.com/word"
r = requests.get(url=URL, params={})
data = r.json()
word = data[0].upper()
print(word)
attempts = 10

hangword = ("_ " * len(word)).split(' ')[:-1]
while '_' in hangword:
    if attempts == 0:
        print(f'No attempts left. You lost. The word was {word}')
        break
    print(f'{attempts} attempts left\n')
    print(f"{' '.join(hangword)}\n")
    letter = input('Select a letter: ')
    letter = letter.upper()
    occurences = [m.start() for m in re.finditer(letter, word)]
    if len(occurences) > 0:
        for pos in occurences:
            hangword[pos] = letter
    else:
        attempts = attempts - 1
    os.system('cls')
if '_' not in hangword:
    print('Congrats!')