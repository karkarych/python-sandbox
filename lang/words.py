import http.client

conn = http.client.HTTPSConnection("wordsapiv1.p.rapidapi.com")

headers = {
    'x-rapidapi-host': "wordsapiv1.p.rapidapi.com",
    'x-rapidapi-key': "e87e2264f3msh8d046b74bf37d5ep1e2294jsne68ea0c5b35a"
    }

queryWord = input("Enter word: ")

conn.request("GET", "/words/"+queryWord+"/typeOf", headers=headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
